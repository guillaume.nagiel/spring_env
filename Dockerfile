FROM openjdk:17-jdk-slim

# Mettre à jour les paquets et installer Maven
RUN apt-get update && \
    apt-get install -y maven git curl && \
    rm -rf /var/lib/apt/lists/*

# Dossier de travail
WORKDIR /workspace

# Monter le dossier projet local dans /app
VOLUME /workspace

# Commande par défaut
CMD ["tail", "-f", "/dev/null"]
