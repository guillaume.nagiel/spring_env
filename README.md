# Environnement de Développement Spring Boot

## Description du projet

### Introduction
Ce dépot va vous permettre de déployer un environnement de développement en Java Spring Boot

### Contenu
Dans ce container, vous trouverez des images de :
- [openjdk](https://hub.docker.com/_/openjdk/).

## Prérequis
Pour utiliser ce container, télécharger le logiciel [Docker Desktop](https://docs.docker.com/get-docker/)

Installer les extensions nécessaires dans VS Code :
- Docker
- Dev Containers

## Installation

### Installation du container
Une fois `Docker Desktop` installé, vous pouvez cloner ce dépot.
```
git clone https://gitlab.com/guillaume.nagiel/spring_env.git
```

Puis ouvrez ce dossier avec `VSCode`.

Nous allons maintenant ouvrir cette image docker en tant que `Conteneur de développement` dans `VSCode`:
- Accédez à la palette de commandes en appuyant sur Ctrl+Shift+P (ou Cmd+Shift+P sur macOS).
- Saisissez `Dev Containers: Open Folder in Container`.
- S'il vous est demandé un fichier de configuration, selectionnez `Java Spring Dev Container`.

Le conteneur devrait s'installer avec les extentions `VSCode` pour `Spring`.


### Création d'un projet Spring Boot avec Spring Initializr via VSCode
- Accédez à la palette de commandes en appuyant sur Ctrl+Shift+P (ou Cmd+Shift+P sur macOS).
- Saisissez `Spring Initializr: Generate a Maven Project`.
- Vous avez maintenant accès aux options d'initialisations de votre projet Spring

### Lancement d'un projet Spring Boot
- Accédez à la palette de commandes en appuyant sur Ctrl+Shift+P (ou Cmd+Shift+P sur macOS).
- Saisissez `Spring Boot Dashboard: Connect`.
- Vous avez maintenant accès aux outils `spring` permettant de lancer le projet, les tests et le mode débug, dans la barre d'activité.

# Liste des fonctionnalités à ajouter

### Todo


### In Progress


### Done ✓

- [x] Créer le README.md  
- [x] Créer le TODO.md  